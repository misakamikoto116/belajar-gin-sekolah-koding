package main

import "fmt"

func main4() {
	chann := make(chan string)

	go runChannel("Salam", chann)

	for msg := range chann {
		fmt.Println(msg)
	}
}

// close channel, karena jika tidak dan aplikasi terus berjalan bakal deadlock
func runChannel(text string, c chan string) {
	for i := 0; i < 5; i++ {
		c <- text
	}

	close(c)
}
