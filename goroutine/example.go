package main

import (
	"fmt"
	"time"
)

func printAngka() {
	for i := 0; i < 5; i++ {
		time.Sleep(200 * time.Millisecond)
		fmt.Println(i)
	}
}

func printText2() {
	for i := 0; i < 5; i++ {
		time.Sleep(500 * time.Millisecond)
		fmt.Println("text ", i)
	}
}

// Kalo mau coba jadikan main()
// Kalo Mau tau lebih cepat goroutine
// Kalo gak pake go routine, itu gak bakal pas 3 detik.. tapi bakalan lbh, karena saling tunggu menunggu + 3 detik fungsi main
func main2() {
	start := time.Now() // Mulai Menghitung waktu

	go printAngka()
	go printText2()

	time.Sleep(3000 * time.Millisecond)
	fmt.Println(time.Since(start))
}

// Visualisasi => Mereka berjalan berbarengan, kalo tanpa goroutine akan nunggu print angka dulu, baru text, baru main
// > printAngka ..  ..  ..  ..  ..  ..
// > printText ..     ..    ..   ..   ..
// > main -------------------------------- 3s
