package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

type Article struct {
	Title string `json:"title"`
	Desc  string `json:"desc"`
}

type Articles []Article

var articles = Articles{
	Article{Title: "Judul Pertama", Desc: "Deskripsi Pertama"},
	Article{Title: "Judul Kedua", Desc: "Deskripsi Kedua"},
}

func main() {
	http.HandleFunc("/", getHome)
	http.HandleFunc("/articles", getArticles)
	http.HandleFunc("/post-article", withLogging(postArticle))
	http.ListenAndServe("127.0.0.1:8888", nil)
}

func getHome(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Testing kamu berada di home"))
}

func getArticles(w http.ResponseWriter, r *http.Request) {

	// Encode Articles ke json
	json.NewEncoder(w).Encode(articles)

}

func postArticle(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		var newArticle Article
		err := json.NewDecoder(r.Body).Decode(&newArticle)

		if err != nil {
			fmt.Printf("Ada Error", err)
		}

		articles = append(articles, newArticle)
		json.NewEncoder(w).Encode(articles)

	} else {
		http.Error(w, "Invalid Request Method", http.StatusMethodNotAllowed)
	}
}

// ioutil.ReadAll(r.body).. membaca semua yang ada di body payload
// func postArticle(w http.ResponseWriter, r *http.Request) {
// 	if r.Method == "POST" {
// 		body, err := ioutil.ReadAll(r.Body)
// 		if err != nil {
// 			http.Error(w, "Can't read body", http.StatusInternalServerError)
// 		}

// 		responseBody := string(body)

// 		w.Write([]byte(responseBody))
// 	} else {
// 		http.Error(w, "Invalid Request Method", http.StatusMethodNotAllowed)
// 	}
// }

func withLogging(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		log.Printf("Logged Koneksi dari", r.RemoteAddr)

		next.ServeHTTP(w, r)
	}
}
