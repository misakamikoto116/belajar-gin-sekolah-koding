package main

import (
	"gin-sekolah-koding/config"
	"gin-sekolah-koding/routes"

	"github.com/gin-gonic/gin"
	"github.com/subosito/gotenv"
)

func main() {
	config.InitDB()
	defer config.DB.Close()
	gotenv.Load()

	router := gin.Default()

	v1 := router.Group("api/v1")

	// Auth
	auth := v1.Group("auth")
	auth.GET("auth/:provider", routes.RedirectHandler)
	auth.GET("auth/:provider/callback", routes.CallbackHandler)

	// Articles
	articles := v1.Group("articles")
	articles.GET("", routes.GetHome)
	articles.GET(":slug", routes.GetArticle)
	articles.POST("", routes.PostArticle)

	router.Run("127.0.0.1:8888")
}
