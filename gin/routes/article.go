package routes

import (
	"gin-sekolah-koding/config"
	"gin-sekolah-koding/models"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/gosimple/slug"
)

func GetHome(c *gin.Context) {
	items := []models.Article{}
	config.DB.Find(&items)

	c.JSON(http.StatusOK, gin.H{
		"status": "berhasil ke halaman home",
		"data":   items,
	})
}

func GetArticle(c *gin.Context) {
	slug := c.Param("slug")

	var item models.Article

	if config.DB.First(&item, "slug = ?", slug).RecordNotFound() {
		c.JSON(http.StatusNotFound, gin.H{"status": "error", "message": "record not found"})
		c.Abort()
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"status": "berhasil",
		"data":   item,
	})

}

func PostArticle(c *gin.Context) {
	item := models.Article{
		Title: c.PostForm("title"),
		Desc:  c.PostForm("desc"),
		Slug:  slug.Make(c.PostForm("title")),
	}

	config.DB.Create(&item)

	c.JSON(http.StatusOK, gin.H{
		"status": "berhasil ngepost",
		"data":   item,
	})
}
